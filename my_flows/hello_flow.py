from prefect import Flow, task

@task
def hello_world():
    print('hello world')
    return True 

# simple 1 task flow
with Flow('hello-world') as flow:
   hello_world()


# for local execution
if __name__ == '__main__':
    flow.run()