#!/usr/bin/env python
import os
from git import Repo

from prefect.client import Client
from prefect.storage import Git

import my_flows.hello_flow as flow

# variables from context
prefect_tenant_slug = os.environ["PREFECT_TENANT"]
prefect_project_name = os.environ["PREFECT_PROJECT_NAME"]
repo_host = os.environ["GIT_HOSTNAME"]
repo = os.environ["GIT_REPO_NAME"]
git_token_secret_name = os.environ["GIT_TOKEN_SECRET_NAME"]
git_token_username = os.environ["GIT_TOKEN_USERNAME"]

# presidence for identifing where to find the flow code
# BUILD_TAG => GIT_BRANCH => active_branch
build_tag = branch_name = None
build_tag = os.getenv("BUILD_TAG", "")
if build_tag == "":
  branch_name = os.getenv("GIT_BRANCH", "")
  if branch_name == "":
    branch_name = str(Repo(os.getcwd()).active_branch)

# ensure logging into the correct tenant
prefect_client = Client()
# prefect_client.switch_tenant(tenant_slug=prefect_tenant_slug)

storage = Git(
    repo_host=repo_host,
    repo=repo,
    flow_path=f"{flow.__name__.replace('.','/')}.py",
    flow_name=flow.flow.name,
    branch_name=branch_name,
    tag=build_tag,
    git_token_secret_name=git_token_secret_name,
    git_token_username=git_token_username,
)
storage.add_flow(flow.flow)
flow.flow.storage = storage

flow.flow.register(project_name=prefect_project_name, build=False)
